Unpolarized MK
389 polymorphic synonymous changes within species (Ps)
18 polymorphic non-synonymous changes within species (Pn)
34 fixed synonymous changes between species (Ds)
2 fixed non-synonymous changes between species (Dn)
Nucleotide positions:
[2304, 3052]
Amino acid fixed non-synonymous change postions:
[768, 1018]
p-value = 0.672421148506
alpha= 0.2133676092544987