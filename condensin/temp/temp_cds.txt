Command line: [exonerate --model protein2genome --query condensin_protein_seqs/SMC2_prot.fasta --target temp/temp.fasta --ryo forcebeams 
>CO2_Chr2R
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: FBpp0086591 type=protein; loc=2R:complement(join(14852387..14852554,14852016..14852327,14851224..14851953,14850740..14851167,14850413..14850682,14850163..14850356,14848672..14850109)); ID=FBpp0086591; name=SMC2-PA; parent=FBgn0027783,FBtr0087461; dbxref=FlyBase:FBpp0086591,FlyBase_Annotation_IDs:CG10212-PA,GB_protein:AAF58197.1,REFSEQ:NP_610995,GB_protein:AAF58197,UniProt/TrEMBL:Q7KK96,FlyMine:FBpp0086591,modMine:FBpp0086591; MD5=3a04748e6b2ab8c9ae791d06503589c6; length=1179; release=r6.22; species=Dmel; 
        Target: CO2_Chr2R [revcomp]
         Model: protein2genome:local
     Raw score: 5855
   Query range: 0 -> 1179
  Target range: 4741 -> 861

    1 : MetTyrValLysLysLeuValLeuAspGlyPheLysSerTyrGlyArgArgThrGluIleGl :   21
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        MetTyrValLysLysLeuValLeuAspGlyPheLysSerTyrGlyArgArgThrGluIleGl
 4741 : ATGTATGTTAAGAAGCTAGTGCTCGACGGCTTCAAATCCTATGGCCGGCGCACGGAGATAGA : 4681

   22 : uGlyPheAspProGluPheThrAlaIleThrGlyLeuAsnGlySerGlyLysSerAsnIleL :   42
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        uGlyPheAspProGluPheThrAlaIleThrGlyLeuAsnGlySerGlyLysSerAsnIleL
 4680 : AGGATTCGATCCAGAATTTACGGCTATCACGGGTCTAAACGGCTCCGGCAAGTCAAACATCT : 4618

   43 : euAspSerIleCysPheValLeuGlyIleSerAsnLeuGlnAsn  >>>> Target Intr :   57
        ||||||||||||||||||||||||||||||||||||||||||||            59 bp 
        euAspSerIleCysPheValLeuGlyIleSerAsnLeuGlnAsn++                
 4617 : TGGATAGCATTTGTTTCGTACTGGGAATTAGTAATCTCCAGAATgt................ : 4571

   58 : on 1 >>>>  ValArgAlaSerAlaLeuGlnAspLeuValTyrLysAsnGlyGlnAlaGly :   73
                   |||||||||||||||||||||||||||||||||||||||||||||||||||
                 ++ValArgAlaSerAlaLeuGlnAspLeuValTyrLysAsnGlyGlnAlaGly
 4570 : .........agGTGCGCGCTTCGGCTCTACAGGATTTGGTGTACAAGAATGGGCAGGCGGGC : 4466

   74 : IleThrLysAlaThrValThrIleValPheAspAsnThrAsnProAlaGlnCysProGlnGl :   94
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        IleThrLysAlaThrValThrIleValPheAspAsnThrAsnProAlaGlnCysProGlnGl
 4465 : ATTACCAAGGCCACTGTTACCATTGTGTTTGACAACACAAATCCCGCCCAGTGTCCGCAGGG : 4403

   95 : yTyrGluLysCysArgGluIleSerValThrArgGlnValValValGlyGlyLysAsnLysP :  115
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        yTyrGluLysCysArgGluIleSerValThrArgGlnValValValGlyGlyLysAsnLysP
 4402 : CTACGAAAAGTGCCGGGAGATCTCTGTGACGCGCCAGGTGGTTGTCGGAGGCAAGAACAAGT : 4340

  116 : heLeuIleAsnGlyLysLeuValGlnAsnLysLysValGlnAspPhePheCysSerIleGln :  135
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        heLeuIleAsnGlyLysLeuValGlnAsnLysLysValGlnAspPhePheCysSerIleGln
 4339 : TCCTCATCAATGGCAAGCTGGTGCAGAACAAAAAGGTGCAAGACTTTTTCTGCTCCATCCAG : 4280

  136 : LeuAsnValAsnAsnProAsnPheLeuIleMetGlnGlyLysIleGlnGlnValLeuAsnMe :  156
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LeuAsnValAsnAsnProAsnPheLeuIleMetGlnGlyLysIleGlnGlnValLeuAsnMe
 4279 : CTAAATGTAAACAATCCCAACTTCCTGATCATGCAGGGCAAGATCCAACAAGTGCTAAATAT : 4217

  157 : tLysProLysGlu  >>>> Target Intron 2 >>>>  ValLeuSerMetValGluGl :  167
        |||||||||||||            62 bp            ||||||||||||||||||||
        tLysProLysGlu++                         ++ValLeuSerMetValGluGl
 4216 : GAAGCCCAAGGAGgt.........................agGTTCTGTCAATGGTAGAGGA : 4122

  168 : uAlaAlaGlyThrSerGlnTyrLysThrLysArgAspAlaThrLysThrLeuIleGluLysL :  188
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        uAlaAlaGlyThrSerGlnTyrLysThrLysArgAspAlaThrLysThrLeuIleGluLysL
 4121 : GGCAGCGGGAACCAGTCAGTACAAAACCAAGCGTGATGCCACCAAAACCCTGATTGAGAAAA : 4059

  189 : ysGluThrLysValArgGluThrLysValLeuLeuAspGluGluValLeuProLysLeuVal :  208
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        ysGluThrLysValArgGluThrLysValLeuLeuAspGluGluValLeuProLysLeuVal
 4058 : AGGAGACCAAAGTGCGGGAGACCAAGGTCTTGCTCGACGAGGAGGTGCTGCCAAAGCTGGTT : 3999

  209 : LysLeuArgGlnGluArgSerAlaTyrGlnGluTyrGlnLysIleCysArgAspIleAspPh :  229
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LysLeuArgGlnGluArgSerAlaTyrGlnGluTyrGlnLysIleCysArgAspIleAspPh
 3998 : AAGCTGCGCCAAGAGCGCTCCGCCTACCAGGAGTATCAGAAAATCTGCCGCGACATAGACTT : 3936

  230 : eLeuIleArgIleHisIleSerAlaLysTyrLeuLysGlnCysGluThrLeuLysThrValG :  250
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        eLeuIleArgIleHisIleSerAlaLysTyrLeuLysGlnCysGluThrLeuLysThrValG
 3935 : TCTCATCAGGATTCACATCTCCGCCAAGTATCTCAAGCAATGCGAGACCCTCAAGACGGTGG : 3873

  251 : luAlaAsnGluHisLysIleGluAspArgIleAlaAsnCysLysAlaThrHisAlaLysAsn :  270
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        luAlaAsnGluHisLysIleGluAspArgIleAlaAsnCysLysAlaThrHisAlaLysAsn
 3872 : AGGCCAATGAGCACAAAATCGAGGATCGCATAGCGAACTGCAAGGCCACTCACGCCAAGAAC : 3813

  271 : LeuAlaGluValGluSerIleGluAsnSerValLysGluMetGlnGlnGlnIleAspAlaGl :  291
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LeuAlaGluValGluSerIleGluAsnSerValLysGluMetGlnGlnGlnIleAspAlaGl
 3812 : CTCGCAGAGGTCGAGAGCATTGAAAACTCCGTGAAGGAGATGCAGCAACAGATCGATGCAGA : 3750

  292 : uMetGlyGlySerIleLysAsnLeuGluThrGlnLeuSerAlaLysArgAlaLeuGluAlaT :  312
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        uMetGlyGlySerIleLysAsnLeuGluThrGlnLeuSerAlaLysArgAlaLeuGluAlaT
 3749 : GATGGGTGGCTCCATCAAAAATCTCGAAACCCAGCTCAGCGCCAAGCGGGCACTAGAAGCTA : 3687

  313 : hrAlaThrGlySerLeuLysAlaAlaGlnGlyThrIleGlnGlnAspGluLysLysIleArg :  332
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        hrAlaThrGlySerLeuLysAlaAlaGlnGlyThrIleGlnGlnAspGluLysLysIleArg
 3686 : CCGCTACGGGTAGCTTGAAGGCGGCCCAGGGAACGATTCAGCAAGATGAGAAGAAAATTCGC : 3627

  333 : MetAlaSerLysAsnIleGluAspAspGluArgAlaLeuAlaLysLysGluAlaAspMetAl :  353
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        MetAlaSerLysAsnIleGluAspAspGluArgAlaLeuAlaLysLysGluAlaAspMetAl
 3626 : ATGGCATCCAAAAACATTGAGGATGATGAACGAGCCCTGGCAAAAAAAGAAGCGGACATGGC : 3564

  354 : aLysValGlnGlyGluPheGluSerLeuLysGluAlaAspAlaArgAspSerLysAlaTyrG :  374
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        aLysValGlnGlyGluPheGluSerLeuLysGluAlaAspAlaArgAspSerLysAlaTyrG
 3563 : CAAGGTTCAAGGCGAATTTGAAAGCCTCAAGGAGGCAGATGCCAGGGACTCAAAAGCCTATG : 3501

  375 : luAspAlaGlnLysLysLeuGluAlaValSerGlnGlyLeuSerThrAsnGluAsnGlyGlu :  394
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        luAspAlaGlnLysLysLeuGluAlaValSerGlnGlyLeuSerThrAsnGluAsnGlyGlu
 3500 : AGGACGCCCAAAAGAAGCTGGAGGCTGTCTCGCAGGGACTCTCCACCAATGAAAACGGAGAA : 3441

  395 : AlaSerThrLeuGlnGluGlnLeuIle{V}  >>>> Target Intron 3 >>>>  {al :  404
        |||||||||||||||||||||||||||{|}            56 bp            {||
        AlaSerThrLeuGlnGluGlnLeuIle{V}++                         ++{al
 3440 : GCCTCAACACTGCAAGAACAATTGATA{G}gt.........................ag{TG : 3354

  405 : }AlaLysGluGlnPheSerGluAlaGlnThrThrIleLysThrSerGluIleGluLeuArgH :  425
        }|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        }AlaLysGluGlnPheSerGluAlaGlnThrThrIleLysThrSerGluIleGluLeuArgH
 3353 : }GCCAAGGAGCAGTTCAGCGAGGCACAGACCACCATTAAGACCTCAGAGATCGAGCTGCGTC : 3292

  426 : isThrArgGlyValLeuLysGlnArgGluGlyGluThrGlnThrAsnAspAlaAlaTyrVal :  445
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        isThrArgGlyValLeuLysGlnArgGluGlyGluThrGlnThrAsnAspAlaAlaTyrVal
 3291 : ATACGCGTGGCGTATTGAAACAGCGTGAGGGTGAGACCCAGACCAACGACGCTGCCTATGTA : 3232

  446 : LysAspLysLysLeuHisAspGlnLeuValValGluIleLysAsnLeuGluArgGlnLeuGl :  466
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LysAspLysLysLeuHisAspGlnLeuValValGluIleLysAsnLeuGluArgGlnLeuGl
 3231 : AAGGATAAGAAGCTGCATGATCAGTTGGTGGTTGAGATTAAGAACTTAGAACGGCAGCTGCA : 3169

  467 : nSerLeuAspTyrGluGlyGlyHisPheGluLysLeuLysGlnArgArgAsnAspLeuHisM :  487
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        nSerLeuAspTyrGluGlyGlyHisPheGluLysLeuLysGlnArgArgAsnAspLeuHisM
 3168 : AAGTCTCGATTACGAGGGCGGTCATTTCGAGAAGCTAAAGCAGCGACGCAATGATCTGCACA : 3106

  488 : etArgLysArgAspLeuLysArgGluLeuAspArgCysAsnAlaSerArgTyrAspLeuGln :  507
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        etArgLysArgAspLeuLysArgGluLeuAspArgCysAsnAlaSerArgTyrAspLeuGln
 3105 : TGCGAAAGCGCGATCTGAAACGAGAGCTGGACCGCTGCAATGCCTCCCGCTATGATCTGCAA : 3046

  508 : TyrGlnAspProGluProAsnPheAspArgArgLysValArgGlyLeuValGlyLysLeuPh :  528
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        TyrGlnAspProGluProAsnPheAspArgArgLysValArgGlyLeuValGlyLysLeuPh
 3045 : TACCAGGACCCAGAACCAAACTTTGATCGGCGTAAGGTGCGTGGTTTGGTAGGCAAGTTGTT : 2983

  529 : eGlnValLysAspMetGlnAsnSerMetAlaLeuValGlnThrAlaGlyGlySer  >>>>  :  547
        |||||||||||||||||||||||||||||||||||||||||||||||||||||||       
        eGlnValLysAspMetGlnAsnSerMetAlaLeuValGlnThrAlaGlyGlySer++     
 2982 : CCAGGTGAAAGACATGCAAAACTCCATGGCTCTAGTCCAGACAGCCGGCGGAAGTgt..... : 2924

  548 : Target Intron 4 >>>>  LeuTyrSerTyrValThrAspAspAspValThrSerLysL :  560
             57 bp            ||||||||||||||||||||||||||||||||||||||||
                            ++LeuTyrSerTyrValThrAspAspAspValThrSerLysL
 2923 : ....................agTTATATAGCTACGTGACGGACGATGATGTGACTAGCAAGA : 2830

  561 : ysIleLeuGlnArgGlyAsnLeuGlnArgArgValThrLeuIleProIleAsnLysIleGln :  580
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        ysIleLeuGlnArgGlyAsnLeuGlnArgArgValThrLeuIleProIleAsnLysIleGln
 2829 : AGATCTTGCAGAGGGGCAATCTTCAGCGTCGTGTCACTTTGATTCCCATCAACAAGATTCAA : 2770

  581 : SerGlySerLeuAsnArgAsnValValGluTyrAlaGlnAsnLysValGlyAlaGluAsnVa :  601
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        SerGlySerLeuAsnArgAsnValValGluTyrAlaGlnAsnLysValGlyAlaGluAsnVa
 2769 : TCTGGCTCGCTAAACAGAAATGTGGTAGAATACGCTCAAAACAAAGTGGGCGCCGAGAATGT : 2707

  602 : lGlnTrpAlaMetSerLeuIleAspTyrAspArgTyrTyrGluProValMetLysPheCysP :  622
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        lGlnTrpAlaMetSerLeuIleAspTyrAspArgTyrTyrGluProValMetLysPheCysP
 2706 : CCAATGGGCCATGTCACTGATTGACTATGACCGCTATTACGAGCCTGTCATGAAGTTTTGCT : 2644

  623 : heGlyGlyThrLeuIleCysLysAspLeuIleValAlaLysGln  >>>> Target Intr :  637
        ||||||||||||||||||||||||||||||||||||||||||||            56 bp 
        heGlyGlyThrLeuIleCysLysAspLeuIleValAlaLysGln++                
 2643 : TTGGCGGAACACTTATCTGCAAGGACCTCATTGTAGCCAAACAAgt................ : 2597

  638 : on 5 >>>>  IleSerTyrAspProArgIleAsnCysArgSerValThrLeuGluGlyAsp :  653
                   |||||||||||||||||||||||||||||||||||||||||||||||||||
                 ++IleSerTyrAspProArgIleAsnCysArgSerValThrLeuGluGlyAsp
 2596 : .........agATCAGCTACGACCCCCGTATTAATTGTCGTTCTGTGACTTTGGAAGGAGAT : 2495

  654 : ValValAspProHisGlyThrValSerGlyGlyAlaAlaProLysGlyAlaAsnValLeuGl :  674
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        ValValAspProHisGlyThrValSerGlyGlyAlaAlaProLysGlyAlaAsnValLeuGl
 2494 : GTCGTCGATCCCCATGGCACAGTGTCTGGAGGTGCTGCTCCTAAAGGTGCTAACGTTCTAGA : 2432

  675 : uGluLeuHisAlaIleLysGlnIleGluLysGluTyrArgGluIleAspSerGluIleAlaG :  695
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        uGluLeuHisAlaIleLysGlnIleGluLysGluTyrArgGluIleAspSerGluIleAlaG
 2431 : AGAATTGCACGCAATTAAGCAAATAGAAAAGGAGTACAGGGAGATCGACTCTGAGATAGCCC : 2369

  696 : lnValGluLysGlnIle{Al}  >>>> Target Intron 6 >>>>  {a}SerIleGlu :  704
        |||||||||||||||||{||}            53 bp            {|}|||||||||
        lnValGluLysGlnIle{Al}++                         ++{a}SerIleGlu
 2368 : AAGTGGAAAAGCAAATT{GC}gt.........................ag{A}TCCATTGAA : 2289

  705 : AsnGlnAlaLeuAlaPheAsnLysMetLysGluAsnLeuAspLeuArgGlnHisGluLeuTh :  725
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        AsnGlnAlaLeuAlaPheAsnLysMetLysGluAsnLeuAspLeuArgGlnHisGluLeuTh
 2288 : AACCAGGCCCTCGCGTTCAACAAAATGAAGGAAAATCTTGACTTGCGACAGCACGAGCTAAC : 2226

  726 : rMetCysGluAsnArgLeuAlaGlnThrThrPheGlnGlnAsnGlnAlaGluIleGluGluM :  746
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        rMetCysGluAsnArgLeuAlaGlnThrThrPheGlnGlnAsnGlnAlaGluIleGluGluM
 2225 : CATGTGCGAGAACAGGTTGGCCCAAACAACCTTTCAACAGAATCAGGCTGAAATTGAAGAAA : 2163

  747 : etArgGluArgValLysThrLeuGluGlnGlnIleIleAspSerArgGluLysGlnLysThr :  766
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        etArgGluArgValLysThrLeuGluGlnGlnIleIleAspSerArgGluLysGlnLysThr
 2162 : TGAGGGAAAGGGTCAAGACCCTAGAGCAGCAAATAATTGACTCCCGTGAAAAGCAGAAGACC : 2103

  767 : SerGlnAlaLysIleValAspIleGluAlaLysLeuAlaAspAlaLysGlyTyrArgGluAr :  787
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        SerGlnAlaLysIleValAspIleGluAlaLysLeuAlaAspAlaLysGlyTyrArgGluAr
 2102 : TCGCAAGCCAAAATAGTGGATATCGAGGCGAAGTTGGCAGATGCAAAGGGTTATCGCGAGCG : 2040

  788 : gGluLeuAsnAlaAlaThrAsnGluIleLysValThrLysGlnArgAlaGluLysSerArgA :  808
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        gGluLeuAsnAlaAlaThrAsnGluIleLysValThrLysGlnArgAlaGluLysSerArgA
 2039 : AGAGCTGAATGCAGCCACCAACGAGATCAAGGTGACCAAGCAAAGGGCGGAGAAATCTCGTG : 1977

  809 : laAsnTrpLysLysArgGluGlnGluPheGluThrLeuGlnLeuGluIleThrGluLeuGln :  828
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        laAsnTrpLysLysArgGluGlnGluPheGluThrLeuGlnLeuGluIleThrGluLeuGln
 1976 : CGAACTGGAAAAAGCGAGAGCAGGAGTTCGAAACTCTTCAATTGGAAATTACTGAGCTGCAG : 1917

  829 : LysSerIleGluThrAlaLysLysGlnHisGlnGluMetIleAspAsnLeuGluLysPheLy :  849
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LysSerIleGluThrAlaLysLysGlnHisGlnGluMetIleAspAsnLeuGluLysPheLy
 1916 : AAAAGTATCGAAACTGCAAAGAAGCAGCACCAGGAAATGATTGATAACCTCGAAAAGTTTAA : 1854

  850 : sAlaGluLeuAspAlaLeuLysValAsnSerSerSerAlaAlaSerGluValThrGluLeuG :  870
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        sAlaGluLeuAspAlaLeuLysValAsnSerSerSerAlaAlaSerGluValThrGluLeuG
 1853 : GGCCGAGCTAGACGCTCTGAAAGTAAATAGCTCCAGTGCCGCGTCCGAGGTGACGGAGCTCG : 1791

  871 : luGlnAlaIleLysGluGlnLysAspLysLeuArgAspGlnAsnLysGluMetArgAsnGln :  890
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        luGlnAlaIleLysGluGlnLysAspLysLeuArgAspGlnAsnLysGluMetArgAsnGln
 1790 : AACAGGCGATTAAGGAGCAGAAAGATAAACTGAGAGATCAGAACAAGGAGATGCGAAATCAG : 1731

  891 : LeuValLysLysGluLysMetLeuLysGluAsnGlnGluIleGluLeuGluValLysLysLy :  911
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LeuValLysLysGluLysMetLeuLysGluAsnGlnGluIleGluLeuGluValLysLysLy
 1730 : TTGGTCAAGAAGGAGAAAATGTTGAAGGAAAATCAGGAGATTGAGCTGGAGGTGAAGAAAAA : 1668

  912 : sGluAsnGluGlnLysLysIleSerSerAspAlaLysGluAlaLysLysArgMetGluAlaL :  932
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        sGluAsnGluGlnLysLysIleSerSerAspAlaLysGluAlaLysLysArgMetGluAlaL
 1667 : AGAGAACGAACAAAAAAAGATCAGTTCGGATGCCAAAGAGGCAAAGAAACGGATGGAAGCCC : 1605

  933 : euGluAlaLysTyrProTrpIleProGluGluLysAsnCysPheGlyMetLysAsnThrArg :  952
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        euGluAlaLysTyrProTrpIleProGluGluLysAsnCysPheGlyMetLysAsnThrArg
 1604 : TTGAGGCAAAGTATCCGTGGATTCCGGAGGAGAAGAACTGCTTTGGCATGAAGAATACTCGG : 1545

  953 : TyrAspTyrSerLysGluAspProHisGluAlaGlyAsnLysLeuAlaLysMetGlnGluLy :  973
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        TyrAspTyrSerLysGluAspProHisGluAlaGlyAsnLysLeuAlaLysMetGlnGluLy
 1544 : TACGATTACAGCAAGGAGGACCCCCACGAAGCAGGCAACAAGCTGGCAAAGATGCAGGAGAA : 1482

  974 : sLysAspLysMetGluArgThrLeuAsnMetAsnAlaIleMetValLeuAspArgGluGluG :  994
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        sLysAspLysMetGluArgThrLeuAsnMetAsnAlaIleMetValLeuAspArgGluGluG
 1481 : AAAGGACAAAATGGAACGCACTCTCAACATGAACGCTATCATGGTGCTGGATCGCGAGGAAG : 1419

  995 : luAsnPheLysGluThrGluArgArgArgAsnIleValAlaMetAspLysGluLysIleLys : 1014
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        luAsnPheLysGluThrGluArgArgArgAsnIleValAlaMetAspLysGluLysIleLys
 1418 : AGAACTTCAAGGAGACCGAGCGCCGGCGAAACATTGTGGCGATGGACAAGGAGAAGATTAAG : 1359

 1015 : LysIleIleValLysMetAspGluGluGluGlnAspGlnLeuAsnLysAlaAlaThrGluVa : 1035
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LysIleIleValLysMetAspGluGluGluGlnAspGlnLeuAsnLysAlaAlaThrGluVa
 1358 : AAAATCATAGTGAAGATGGACGAAGAAGAGCAGGATCAGCTGAACAAAGCCGCTACCGAGGT : 1296

 1036 : lAsnThrAsnPheSerGlyIlePheSerSerLeuLeuProGlyAlaGluAlaLysLeuAsnP : 1056
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        lAsnThrAsnPheSerGlyIlePheSerSerLeuLeuProGlyAlaGluAlaLysLeuAsnP
 1295 : GAACACGAACTTCAGTGGCATCTTTAGCTCTCTTTTACCGGGTGCTGAAGCGAAACTCAATC : 1233

 1057 : roValHisThrAsnGlyCysLeuThrGlyLeuGluIleLysValGlyPheAsnGlyIleTrp : 1076
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        roValHisThrAsnGlyCysLeuThrGlyLeuGluIleLysValGlyPheAsnGlyIleTrp
 1232 : CCGTCCATACCAATGGCTGTCTGACCGGTTTGGAGATTAAAGTCGGCTTCAATGGCATATGG : 1173

 1077 : LysGluSerLeuGlyGluLeuSerGlyGlyGlnLysSerLeuValAlaLeuSerLeuValLe : 1097
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LysGluSerLeuGlyGluLeuSerGlyGlyGlnLysSerLeuValAlaLeuSerLeuValLe
 1172 : AAGGAGAGTCTCGGTGAACTTTCTGGTGGCCAGAAGTCCTTGGTGGCTCTGTCTCTTGTCCT : 1110

 1098 : uAlaMetLeuLysPheSerProAlaProLeuTyrIleLeuAspGluValAspAlaAlaLeuA : 1118
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        uAlaMetLeuLysPheSerProAlaProLeuTyrIleLeuAspGluValAspAlaAlaLeuA
 1109 : GGCCATGCTTAAGTTCTCTCCGGCTCCCTTGTACATTTTGGATGAGGTGGACGCTGCCCTGG : 1047

 1119 : spMetSerHisThrGlnAsnIleGlySerMetLeuLysGlnHisPheThrAsnSerGlnPhe : 1138
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        spMetSerHisThrGlnAsnIleGlySerMetLeuLysGlnHisPheThrAsnSerGlnPhe
 1046 : ACATGTCACACACCCAAAACATAGGTAGTATGTTGAAGCAGCACTTCACCAACTCGCAGTTC :  987

 1139 : LeuIleValSerLeuLysAspGlyLeuPheAsnHisAlaAsnValLeuPheArgThrLeuPh : 1159
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        LeuIleValSerLeuLysAspGlyLeuPheAsnHisAlaAsnValLeuPheArgThrLeuPh
  986 : TTGATTGTGTCTCTCAAGGATGGTCTCTTCAACCACGCCAACGTCCTGTTCCGCACTCTCTT :  924

 1160 : eGluGluGlyValSerThrIleThrArgGlnValSerArgGlnAlaThrThrAsnLysArg : 1179
        |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        eGluGluGlyValSerThrIleThrArgGlnValSerArgGlnAlaThrThrAsnLysArg
  923 : CGAGGAGGGTGTGTCCACCATCACCCGGCAGGTCAGCAGACAGGCGACAACCAACAAACGC :  862

vulgar: FBpp0086591 0 1179 . CO2_Chr2R 4741 861 - 5855 M 56 168 5 0 2 I 0 55 3 0 2 M 104 312 5 0 2 I 0 58 3 0 2 M 243 729 S 0 1 5 0 2 I 0 52 3 0 2 S 1 2 M 142 426 5 0 2 I 0 53 3 0 2 M 90 270 5 0 2 I 0 52 3 0 2 M 64 192 S 0 2 5 0 2 I 0 49 3 0 2 S 1 1 M 478 1434
forcebeams 
>CO2_Chr2R
ATGTATGTTAAGAAGCTAGTGCTCGACGGCTTCAAATCCTATGGCCGGCGCACGGAGATAGAAGGATTCG
ATCCAGAATTTACGGCTATCACGGGTCTAAACGGCTCCGGCAAGTCAAACATCTTGGATAGCATTTGTTT
CGTACTGGGAATTAGTAATCTCCAGAATGTGCGCGCTTCGGCTCTACAGGATTTGGTGTACAAGAATGGG
CAGGCGGGCATTACCAAGGCCACTGTTACCATTGTGTTTGACAACACAAATCCCGCCCAGTGTCCGCAGG
GCTACGAAAAGTGCCGGGAGATCTCTGTGACGCGCCAGGTGGTTGTCGGAGGCAAGAACAAGTTCCTCAT
CAATGGCAAGCTGGTGCAGAACAAAAAGGTGCAAGACTTTTTCTGCTCCATCCAGCTAAATGTAAACAAT
CCCAACTTCCTGATCATGCAGGGCAAGATCCAACAAGTGCTAAATATGAAGCCCAAGGAGGTTCTGTCAA
TGGTAGAGGAGGCAGCGGGAACCAGTCAGTACAAAACCAAGCGTGATGCCACCAAAACCCTGATTGAGAA
AAAGGAGACCAAAGTGCGGGAGACCAAGGTCTTGCTCGACGAGGAGGTGCTGCCAAAGCTGGTTAAGCTG
CGCCAAGAGCGCTCCGCCTACCAGGAGTATCAGAAAATCTGCCGCGACATAGACTTTCTCATCAGGATTC
ACATCTCCGCCAAGTATCTCAAGCAATGCGAGACCCTCAAGACGGTGGAGGCCAATGAGCACAAAATCGA
GGATCGCATAGCGAACTGCAAGGCCACTCACGCCAAGAACCTCGCAGAGGTCGAGAGCATTGAAAACTCC
GTGAAGGAGATGCAGCAACAGATCGATGCAGAGATGGGTGGCTCCATCAAAAATCTCGAAACCCAGCTCA
GCGCCAAGCGGGCACTAGAAGCTACCGCTACGGGTAGCTTGAAGGCGGCCCAGGGAACGATTCAGCAAGA
TGAGAAGAAAATTCGCATGGCATCCAAAAACATTGAGGATGATGAACGAGCCCTGGCAAAAAAAGAAGCG
GACATGGCCAAGGTTCAAGGCGAATTTGAAAGCCTCAAGGAGGCAGATGCCAGGGACTCAAAAGCCTATG
AGGACGCCCAAAAGAAGCTGGAGGCTGTCTCGCAGGGACTCTCCACCAATGAAAACGGAGAAGCCTCAAC
ACTGCAAGAACAATTGATAGTGGCCAAGGAGCAGTTCAGCGAGGCACAGACCACCATTAAGACCTCAGAG
ATCGAGCTGCGTCATACGCGTGGCGTATTGAAACAGCGTGAGGGTGAGACCCAGACCAACGACGCTGCCT
ATGTAAAGGATAAGAAGCTGCATGATCAGTTGGTGGTTGAGATTAAGAACTTAGAACGGCAGCTGCAAAG
TCTCGATTACGAGGGCGGTCATTTCGAGAAGCTAAAGCAGCGACGCAATGATCTGCACATGCGAAAGCGC
GATCTGAAACGAGAGCTGGACCGCTGCAATGCCTCCCGCTATGATCTGCAATACCAGGACCCAGAACCAA
ACTTTGATCGGCGTAAGGTGCGTGGTTTGGTAGGCAAGTTGTTCCAGGTGAAAGACATGCAAAACTCCAT
GGCTCTAGTCCAGACAGCCGGCGGAAGTTTATATAGCTACGTGACGGACGATGATGTGACTAGCAAGAAG
ATCTTGCAGAGGGGCAATCTTCAGCGTCGTGTCACTTTGATTCCCATCAACAAGATTCAATCTGGCTCGC
TAAACAGAAATGTGGTAGAATACGCTCAAAACAAAGTGGGCGCCGAGAATGTCCAATGGGCCATGTCACT
GATTGACTATGACCGCTATTACGAGCCTGTCATGAAGTTTTGCTTTGGCGGAACACTTATCTGCAAGGAC
CTCATTGTAGCCAAACAAATCAGCTACGACCCCCGTATTAATTGTCGTTCTGTGACTTTGGAAGGAGATG
TCGTCGATCCCCATGGCACAGTGTCTGGAGGTGCTGCTCCTAAAGGTGCTAACGTTCTAGAAGAATTGCA
CGCAATTAAGCAAATAGAAAAGGAGTACAGGGAGATCGACTCTGAGATAGCCCAAGTGGAAAAGCAAATT
GCATCCATTGAAAACCAGGCCCTCGCGTTCAACAAAATGAAGGAAAATCTTGACTTGCGACAGCACGAGC
TAACCATGTGCGAGAACAGGTTGGCCCAAACAACCTTTCAACAGAATCAGGCTGAAATTGAAGAAATGAG
GGAAAGGGTCAAGACCCTAGAGCAGCAAATAATTGACTCCCGTGAAAAGCAGAAGACCTCGCAAGCCAAA
ATAGTGGATATCGAGGCGAAGTTGGCAGATGCAAAGGGTTATCGCGAGCGAGAGCTGAATGCAGCCACCA
ACGAGATCAAGGTGACCAAGCAAAGGGCGGAGAAATCTCGTGCGAACTGGAAAAAGCGAGAGCAGGAGTT
CGAAACTCTTCAATTGGAAATTACTGAGCTGCAGAAAAGTATCGAAACTGCAAAGAAGCAGCACCAGGAA
ATGATTGATAACCTCGAAAAGTTTAAGGCCGAGCTAGACGCTCTGAAAGTAAATAGCTCCAGTGCCGCGT
CCGAGGTGACGGAGCTCGAACAGGCGATTAAGGAGCAGAAAGATAAACTGAGAGATCAGAACAAGGAGAT
GCGAAATCAGTTGGTCAAGAAGGAGAAAATGTTGAAGGAAAATCAGGAGATTGAGCTGGAGGTGAAGAAA
AAAGAGAACGAACAAAAAAAGATCAGTTCGGATGCCAAAGAGGCAAAGAAACGGATGGAAGCCCTTGAGG
CAAAGTATCCGTGGATTCCGGAGGAGAAGAACTGCTTTGGCATGAAGAATACTCGGTACGATTACAGCAA
GGAGGACCCCCACGAAGCAGGCAACAAGCTGGCAAAGATGCAGGAGAAAAAGGACAAAATGGAACGCACT
CTCAACATGAACGCTATCATGGTGCTGGATCGCGAGGAAGAGAACTTCAAGGAGACCGAGCGCCGGCGAA
ACATTGTGGCGATGGACAAGGAGAAGATTAAGAAAATCATAGTGAAGATGGACGAAGAAGAGCAGGATCA
GCTGAACAAAGCCGCTACCGAGGTGAACACGAACTTCAGTGGCATCTTTAGCTCTCTTTTACCGGGTGCT
GAAGCGAAACTCAATCCCGTCCATACCAATGGCTGTCTGACCGGTTTGGAGATTAAAGTCGGCTTCAATG
GCATATGGAAGGAGAGTCTCGGTGAACTTTCTGGTGGCCAGAAGTCCTTGGTGGCTCTGTCTCTTGTCCT
GGCCATGCTTAAGTTCTCTCCGGCTCCCTTGTACATTTTGGATGAGGTGGACGCTGCCCTGGACATGTCA
CACACCCAAAACATAGGTAGTATGTTGAAGCAGCACTTCACCAACTCGCAGTTCTTGATTGTGTCTCTCA
AGGATGGTCTCTTCAACCACGCCAACGTCCTGTTCCGCACTCTCTTCGAGGAGGGTGTGTCCACCATCAC
CCGGCAGGTCAGCAGACAGGCGACAACCAACAAACGC

-- completed exonerate analysis
