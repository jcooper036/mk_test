Unpolarized MK
357 polymorphic synonymous changes within species (Ps)
5 polymorphic non-synonymous changes within species (Pn)
32 fixed synonymous changes between species (Ds)
3 fixed non-synonymous changes between species (Dn)
Nucleotide positions:
[2663, 2863, 3186]
Amino acid fixed non-synonymous change postions:
[888, 955, 1062]
p-value = 0.025904676192
alpha= 0.8506069094304388