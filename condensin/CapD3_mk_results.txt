Unpolarized MK
544 polymorphic synonymous changes within species (Ps)
231 polymorphic non-synonymous changes within species (Pn)
25 fixed synonymous changes between species (Ds)
53 fixed non-synonymous changes between species (Dn)
Nucleotide positions:
[9, 25, 52, 59, 103, 104, 107, 139, 154, 208, 209, 211, 225, 275, 310, 316, 338, 340, 422, 424, 460, 505, 541, 551, 555, 688, 706, 853, 998, 1514, 1824, 2156, 2296, 2297, 2320, 2872, 2873, 2936, 3072, 3119, 3260, 3299, 3317, 3319, 3334, 3418, 3440, 3497, 3520, 3589, 3652, 3666, 3718]
Amino acid fixed non-synonymous change postions:
[3, 9, 18, 20, 35, 35, 36, 47, 52, 70, 70, 71, 75, 92, 104, 106, 113, 114, 141, 142, 154, 169, 181, 184, 185, 230, 236, 285, 333, 505, 608, 719, 766, 766, 774, 958, 958, 979, 1024, 1040, 1087, 1100, 1106, 1107, 1112, 1140, 1147, 1166, 1174, 1197, 1218, 1222, 1240]
p-value = 9.56116972425e-11
alpha= 0.7997017203107658