#!/usr/bin/env python3
"""Usage: Mk_main.py [options] GENE FILE1 FILE2 FILE3

Arguments:
    gene   gene name
    FILE1  FASTA with all sequences from PopFly (mel)
    FILE2  FASTA with sim sequences (sim)
    FILE3  FASTA with mel protein sequence

Options:
    --help         Prints help message (default: no)
"""

# This script processes sequences from PopFly https://popfly.uab.cat/
# It will take two files of sequences (nt), an amino acid sequence, and go through a process of exonertate, alignment, backtranslation, and trimming to
# generate sequences for MK tests that are all the same length
# The output is two files, one fasta with all trimmed samples, and one .ctl control file for the MK script
# Requires exonertae and Clustal Omega



## tools
import docopt
import os
import os.path
from Bio import SeqIO
import subprocess

# intitaion for docopt
if __name__ == '__main__':

    try:
        arguments = docopt.docopt(__doc__)
        needhelp = arguments['--help']
        gene_name = str(arguments['GENE'])
        mel_sequence_file = str(arguments['FILE1'])
        sim_sequence_file = str(arguments['FILE2'])
        protein_file = str(arguments['FILE3'])

    except docopt.DocoptExit as e:
        print(e)


##############################
## classes
##############################

class Seq(object):
    """
    Container class for sequence objects
    """
    def __init__(self, name):
        self.name = name
        self.species = None
        self.gene = None
        self.nt_seq = None
        self.exoner_seq = None
        self.exoner_trans = None

##############################
## definintions
##############################
def trans(sequence):
    # takes a nucleotide string and returns the translation as an amino acid string

    # codon table
    transtable = {
        'agg' : 'R',
        'aga' : 'R',
        'agc' : 'S',
        'agt' : 'S',
        'aag' : 'K',
        'aaa' : 'K',
        'aac' : 'N',
        'aat' : 'N',
        'aca' : 'T',
        'acc' : 'T',
        'acg' : 'T',
        'act' : 'T',
        'atg' : 'M',
        'ata' : 'I',
        'atc' : 'I',
        'att' : 'I',
        'cgg' : 'R',
        'cga' : 'R',
        'cgc' : 'R',
        'cgt' : 'R',
        'cag' : 'Q',
        'caa' : 'Q',
        'cac' : 'H',
        'cat' : 'H',
        'ccg' : 'P',
        'cca' : 'P',
        'ccc' : 'P',
        'cct' : 'P',
        'ctg' : 'L',
        'cta' : 'L',
        'ctc' : 'L',
        'ctt' : 'L',
        'tgg' : 'W',
        'tga' : 'X',
        'tgc' : 'C',
        'tgt' : 'C',
        'tag' : 'X',
        'taa' : 'X',
        'tac' : 'Y',
        'tat' : 'Y',
        'tcg' : 'S',
        'tca' : 'S',
        'tcc' : 'S',
        'tct' : 'S',
        'ttg' : 'L',
        'tta' : 'L',
        'ttc' : 'F',
        'ttt' : 'F',
        'ggg' : 'G',
        'gga' : 'G',
        'ggc' : 'G',
        'ggt' : 'G',
        'gag' : 'E',
        'gaa' : 'E',
        'gac' : 'D',
        'gat' : 'D',
        'gcg' : 'A',
        'gca' : 'A',
        'gcc' : 'A',
        'gct' : 'A',
        'gtg' : 'V',
        'gta' : 'V',
        'gtc' : 'V',
        'gtt' : 'V'}

    # this is a counting variable
    k = 3

    # this the blank string for the amino acid sequence
    aaseq = ''

    # stops when it gets to the end of the sequence
    while k <= len(sequence):
        # try to add a codon. will reject because it won't find the key if the variable is blank
        try:
            aaseq += transtable[sequence[(k-3):k].lower()]
        except:
            print('ERROR! Translation failed.')

        # count up 3
        k = k + 3

    # return value is a string of amino acids
    return aaseq

def exoner(sequence):
    """
    Takes a sequence object from the SEQS dictionary
    Uses exonerate (local installation) to search for the amino acid sequence in a nucleotie string
    Returns the CDS sequence
    """
    
    ## make the temp fasta file of the nucleotide sequence
    with open ('temp/temp.fasta', 'w') as f:
        f.write('>' + sequence.name + '\n')
        f.write(sequence.nt_seq)

    # exonerate command
    subprocess.check_output('exonerate --model protein2genome --query ' + protein_file + ' --target temp/temp.fasta --ryo "forcebeams \n>' + sequence.name + '\n%tcs\n" --bestn 1 >temp/temp_cds.txt' ,shell=True)

    exer_seq = []
    
    # read the exonerate output
    with open ('temp/temp_cds.txt', 'r') as f:
        copy = False
        for line in f:
            ## conditions under which to copy
            if line.strip() == 'forcebeams':
                copy = True
            elif line.strip() == '-- completed exonerate analysis':
                copy = False
            elif line.strip() == 'C4 Alignment:':
                copy = False            

            # and if it should be copied
            elif copy:
                if '>' not in line:
                    # line = [x.strip() for x in line]
                    # line = ''.join(line) #@ Are these two lines necessary if file is being read line by line? Could just use str(line.strip())
                    exer_seq.append(line.strip())
    
    ## put it together, and only add the sequence if there are not Ns
    exer_seq = ''.join(exer_seq)
    if 'N' in exer_seq:
        pass
    else:    
        sequence.exoner_seq = exer_seq
    
    return sequence

def back_translate(aa_alignment, nuc_input):
    # takes a dictionary that contains amino acid strings, that are already
    # aligned, where stop codons are marked with X or *, that are all the same
    # length, and reduces them to
    # the minimum amino continuous amino acid sequence across all species

    # make sure input is divisable by 3
    for key in nuc_input:
        if not (len(nuc_input[key]) % 3) == 0:
            print('\nWARNING: The nucleotide input for back translation for ' + key + ' is not divisable by 3. Please check.\n')

    minaa = {}
    for key in aa_alignment:
        cond_seq = ""
        for i in range(0, len(aa_alignment[key])):
            if not any(aa_alignment[nkey][i] == "*" or aa_alignment[nkey][i] == "X" or aa_alignment[nkey][i] == "-" for nkey in aa_alignment):
                cond_seq += aa_alignment[key][i]
        minaa[key] = cond_seq

    minnuc = {}
    for key in minaa:
        k = 0
        cond_nuc = nuc_input[key]
        while k < len(minaa[key]):
            if not minaa[key][k] == trans(cond_nuc[(k*3):((k*3)+3)]): #@ translating codon by codon, which means that if there is an error message in the translation def then it will pop up every time
                cond_nuc = cond_nuc[:(k*3)] + cond_nuc[(k*3)+3:]
            else:
                k += 1
        else:
            cond_nuc = cond_nuc[:(k*3)]

        minnuc[key] = cond_nuc
    
    ## and also return the dictionary with the back-translation
    return minnuc


##############################
## calling the functions
##############################

# make a dictionary of sequences
SEQS = {}

# read the fasta files
for record in SeqIO.parse(mel_sequence_file, "fasta"):
    SEQS[record.id] = Seq(record.id)
    SEQS[record.id].nt_seq = str(record.seq)
    SEQS[record.id].species = 'mel'
    SEQS[record.id].gene = gene_name
for record in SeqIO.parse(sim_sequence_file, "fasta"):
    SEQS[record.id] = Seq(record.id)
    SEQS[record.id].nt_seq = str(record.seq)
    SEQS[record.id].species = 'sim'
    SEQS[record.id].gene = gene_name
print('Loaded sequences')

# exonerate everything
for seq in SEQS:
    SEQS[seq] = exoner(SEQS[seq])
print('Exonerated everything')

# purge the sequences that had Ns
pop_list = []
for seq in SEQS:
    if SEQS[seq].exoner_seq == None:
        pop_list.append(seq)
    elif len(SEQS[seq].exoner_seq) == 0:
        pop_list.append(seq)

for seq in pop_list:
    SEQS.pop(seq)
print('Purged bad sequences')

# translate sequences
for seq in SEQS:
    SEQS[seq].exoner_trans = trans(SEQS[seq].exoner_seq)
print('Translated exonerated sequences')

## align the sequences
with open('temp/align_input.txt', 'w') as f:
    for seq in SEQS:
        f.write('>' + str(seq) + '\n')
        f.write(str(SEQS[seq].exoner_trans) + '\n')
subprocess.check_output('clustalo -i temp/align_input.txt -o temp/align_out.txt --force',shell=True)
alignment = {}
for record in SeqIO.parse('temp/align_out.txt', "fasta"):
    alignment[record.id] = str(record.seq)
print('Aligned Sequences')

## back translate to the reduced CDS sequences
exoner_ins = {}
for seq in SEQS:
    exoner_ins[seq] = SEQS[seq].exoner_seq
condensed_nuc = back_translate(alignment, exoner_ins)
mel_seqs = []
sim_seqs = []
for key in condensed_nuc:
    if SEQS[key].species == 'mel':
        mel_seqs.append(key)
    if SEQS[key].species == 'sim':
        sim_seqs.append(key)
print('Back translation complete')

## print everything to files for MK test
gene_file = gene_name + '_mk.fasta'
gene_cont = gene_name + '_mk.ctl'
with open(gene_file, 'w') as gene_f:
    with open(gene_cont, 'w') as cont_f:
        cont_f.write('group1')
        for sample in mel_seqs:
            cont_f.write(',' + str(sample))
            gene_f.write('>' + str(sample) + '\n')
            gene_f.write(str(condensed_nuc[sample]) + '\n')
        cont_f.write('\ngroup2')
        for sample in sim_seqs:
            cont_f.write(',' + str(sample))
            gene_f.write('>' + str(sample) + '\n')
            gene_f.write(str(condensed_nuc[sample]) + '\n')
        cont_f.write('\npolarizer')
print('Wrote MK output files')