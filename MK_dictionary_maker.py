#!/usr/bin/env python3

### turns out this got put into the main file, but there are some good notes
# and things to try here


import os.path
from Bio import SeqIO


seqs = {}
# ask the user where the file is and read it to a variable
print("Input FASTA file location")
#filepath = input()
filepath ="/Users/Jacob/Scripts/MK/gfzf/gfzf_MK_nt_reduced.fasta"
# read the fasta files
for record in SeqIO.parse(filepath, "fasta"):
    #print(record.seq)
    seqs[record.id] = str(record.seq)

# now read the .ctl file that contains the species IDs in groups
print("Input the control file with groups of species")
#ctlfile = input()
ctlfile = "/Users/Jacob/Scripts/MK/gfzf/gfzf.ctl"
group1 = {}
group2 = {}
polarizer = {}
count = 0

# read each group from the species and populate the dictionaries with
# those sequences
with open(ctlfile, "r+") as f:
    for line in f.read().splitlines(): # reads the file without "\n"
        lst = (line.split(","))
        for x in (group1, group2, polarizer):
            # have to do this because checking against the name of the
            # dictionary
            if lst[0] == "group1":
                for n in range(1, len(lst)):
                    group1[lst[n]] = seqs[lst[n]]
            elif lst[0] == "group2":
                for n in range(1, len(lst)):
                    group2[lst[n]] = seqs[lst[n]]
            elif lst[0] == "polarizer":
                for n in range(1, len(lst)):
                    polarizer[lst[n]] = seqs[lst[n]]

print(group1.keys())
print(group2.keys())
print(polarizer.keys())


# ## prints out the keys, asks the user to sort the keys into three groups :
# # Group1, Group2, polarizer
# # will check to make sure the contents of each group exist
#
# # this bit is awkward, but it is to sort the keys in a list so they will be
# # easier to read
# print("These are the seq IDs:")
# ids = []
# for keys in seqs:
#     ids.append(keys)
# ids = sorted(ids)
# for keys in ids:
#     print(keys)
#
# print("\nPlease enter the IDs for Group1 and press 'Enter' after each \nType 'Done' when finished")
# group1l = []
# usr_in = input()
# while any(usr_in == x for x in seqs):
#     group1l.append(usr_in)
#     usr_in = input()
#
# print("\nThese are the remaining seq IDs:")
# for x in ids:
#     if not any(x == r for r in group1l):
#         print(x)
# print("\nPlease enter the IDs for Group2 and press 'Enter' after each \nType 'Done' when finished")
# group2l = []
# usr_in = input()
# while any(usr_in == x for x in seqs):
#     group2l.append(usr_in)
#     usr_in = input()
#
# print("\nThese are the remaining seq IDs:")
# for x in ids:
#     if not any(x == r for r in group1l or x == s for s in group2l):
#         print(x)
# print("\nPlease enter the ID for the polarizing sequence and press 'Enter' after each \nType 'Done' when finished")
# group3l = []
# usr_in = input()
# while any(usr_in == x for x in seqs):
#     group3l.append(usr_in)
#     usr_in = input()
#
# ## make two dictionaries with the sequences and keys
# group1 = {}
# group2 = {}
# polarize = {}
# for key in group1l:
#     group1[key] = seqs[key]
# for key in group2l:
#     group2[key] = seqs[key]
# for key in group3l:
#     if bool(group3l):
#         polarize[key] = seqs[key]
#
# ## ask for outfile name:
# print("What do you want to call the outfile")
# outfl = input()
#
# if bool(group3l):
#     with open("/Users/Jacob/Scripts/MK/" + outfl + ".txt", "w+") as f:
#         f.write("###" + outfl + "\n\n" + group1 + "\n\n" + group2 + "\n\n" + polarizer)
# else:
#     with open("/Users/Jacob/Scripts/MK/" + outfl + ".txt", "w+") as f:
#         f.write("###" + outfl + "\n\n" + group1 + "\n\n" + group2)
# #/Users/Jacob/Desktop/gfzf_MK_nt_reduced.fasta
