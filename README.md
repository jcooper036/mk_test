# README #



### Running MK tests on multiple sequences ###

* This respository contains the code and examples for running McDonald-Krietman tests on multiple sequences.
* v1.00

### Setup ###

* Open the latest python script, check that all the dependencies at the top are installed.
* Dependencies (v1.00)
	* docopt
	* os
	* Bio
	* scipy
	* math
	
* Usage: script_name [options] FILE1 FILE2
* FILE1  FASTA with all sequences. All sequences must be the same length. Must be .fasta
* FILE2  Control file with sequence names. Must be .ctl  See example for details

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* contact jcooper036@gmail.com with questions
* Written while I was a graduate student in Nitin Phadnis's lab at the University of Utah