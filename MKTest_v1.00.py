#!/usr/bin/env python3
"""Usage: Mk_main.py [options] FILE1 FILE2

Arguments:
    FILE1  FASTA with all sequences. All sequences must be the same length. Must be .fasta
    FILE2  Control file with sequence names. Must be .ctl  See example for details
Options:
    --start=N      Specifies start position (nt). Must be the first position of a codon [default: 'all']
    --end=N        Specifies end position (nt). Must be the last position of a codon [default: 'all']
    --help         Prints help message (default: no)
"""


## tools
import docopt
import os
import os.path
from Bio import SeqIO
import scipy.stats as stats
import math

# intitaion for docopt
if __name__ == '__main__':

    try:
        arguments = docopt.docopt(__doc__)
        needhelp = arguments['--help']
        usr_sttpos = arguments['--start']
        usr_enos = arguments['--end']
        filepath = str(arguments['FILE1'])
        ctlfile = str(arguments['FILE2'])

    except docopt.DocoptExit as e:
        print(e)

##############################
## definintions
##############################
def is_it_int (invar):
    try:
        int(invar)
        return int(invar)

    except ValueError:
        pass

def loadfile(filepath, ctlfile):
    # input: fasta file with all sequences the same length, control file that
    #   contains group1, group2, and polarizer (even if empty)
    # reads in all sequences, sorts the groups according to the control file
    # output: three groups as dictionaries
    seqs = {}

    # read the fasta files
    for record in SeqIO.parse(filepath, "fasta"):
        #print(record.seq)
        seqs[record.id] = str(record.seq)

    # define these dictionaries
    group1 = {}
    group2 = {}
    polarizer = {}

    # read each group from the species and populate the dictionaries with
    # those sequences
    with open(ctlfile, "r+") as f:
        for line in f.read().splitlines(): # reads the file without "\n"
            lst = (line.split(","))
            for x in (group1, group2, polarizer):
                # have to do this because checking against the name of the
                # dictionary
                if lst[0] == "group1":
                    for n in range(1, len(lst)):
                        group1[lst[n]] = seqs[lst[n]]
                elif lst[0] == "group2":
                    for n in range(1, len(lst)):
                        group2[lst[n]] = seqs[lst[n]]
                elif lst[0] == "polarizer":
                    for n in range(1, len(lst)):
                        polarizer[lst[n]] = seqs[lst[n]]


    # slicing for different starting and ending positions
    if bool(usr_startpos):

        # make sure that the start codon is good
        if not ((usr_startpos + 2) % 3) == 0:
            print("\nError: Starting position is not the first nucleotide of a codon\n")
            quit()

        # slice each of the sequences. This does NOT change the fasta file
        for x in (group1, group2, polarizer):
            for keyn in x:
                # print (x[keyn][(usr_startpos-1):])
                x[keyn] = x[keyn][(usr_startpos-1):]

    if bool(usr_endpos):
        # make sure that the start codon is good
        if not (usr_endpos % 3) == 0:
            print("\nError: Ending position is not the last nucleotide of a codon\n")
            quit()
        if bool(usr_startpos):
            usr_endadj = usr_endpos - (usr_startpos - 1)
        else:
            usr_endadj = usr_endpos

        # slice each of the sequences. This does NOT change the fasta file
        for x in (group1, group2, polarizer):
            for keyn in x:
                # print (x[keyn][:usr_endadj])
                x[keyn] = x[keyn][:usr_endadj]

    return group1, group2, polarizer

def trans(sequence):
    # takes a nucleotide string and returns the translation as an amino acid string

    # codon table
    transtable = {
        'agg' : 'R',
        'aga' : 'R',
        'agc' : 'S',
        'agt' : 'S',
        'aag' : 'K',
        'aaa' : 'K',
        'aac' : 'N',
        'aat' : 'N',
        'aca' : 'T',
        'acc' : 'T',
        'acg' : 'T',
        'act' : 'T',
        'atg' : 'M',
        'ata' : 'I',
        'atc' : 'I',
        'att' : 'I',
        'cgg' : 'R',
        'cga' : 'R',
        'cgc' : 'R',
        'cgt' : 'R',
        'cag' : 'Q',
        'caa' : 'Q',
        'cac' : 'H',
        'cat' : 'H',
        'ccg' : 'P',
        'cca' : 'P',
        'ccc' : 'P',
        'cct' : 'P',
        'ctg' : 'L',
        'cta' : 'L',
        'ctc' : 'L',
        'ctt' : 'L',
        'tgg' : 'W',
        'tga' : 'X',
        'tgc' : 'C',
        'tgt' : 'C',
        'tag' : 'X',
        'taa' : 'X',
        'tac' : 'Y',
        'tat' : 'Y',
        'tcg' : 'S',
        'tca' : 'S',
        'tcc' : 'S',
        'tct' : 'S',
        'ttg' : 'L',
        'tta' : 'L',
        'ttc' : 'F',
        'ttt' : 'F',
        'ggg' : 'G',
        'gga' : 'G',
        'ggc' : 'G',
        'ggt' : 'G',
        'gag' : 'E',
        'gaa' : 'E',
        'gac' : 'D',
        'gat' : 'D',
        'gcg' : 'A',
        'gca' : 'A',
        'gcc' : 'A',
        'gct' : 'A',
        'gtg' : 'V',
        'gta' : 'V',
        'gtc' : 'V',
        'gtt' : 'V'}

    # this is a counting variable
    k = 3

    # this the blank string for the amino acid sequence
    aaseq = ''

    # stops when it gets to the end of the sequence
    while k <= len(sequence):
        # try to add a codon. will reject because it won't find the key if the variable is blank
        try:
            aaseq += transtable[sequence[(k-3):k].lower()]
        except:
            print('ERROR! ERROR!')

        # count up 3
        k = k + 3

    # return value is a string of amino acids
    return aaseq

def polysite(seqgroup1, seqgroup2, seqlength):
    # input are two dictionaries of ID : seq, length of the sequence. Output is 6 lists, synonymous polymorphic changes and then non-synonymous polymorphic changes for each species and then combined
    # the two species will not total the combined all the time. This is because sometimes a site has two sets of polymorphic changes, one in each species. Sometimes it just has one set of polymorphic changes

    # counting variable
    k = 3

    # get two sequences (one from each dictionary) together to compare
    compgroup=[]
    compgroup.append(seqgroup1[list(seqgroup1.keys())[0]])
    compgroup.append(seqgroup2[list(seqgroup2.keys())[0]])

    # lists for holding the positions
    poly_syn_g1 = []
    poly_non_g1 = []
    poly_syn_g2 = []
    poly_non_g2 = []
    poly_syn_total = []
    poly_non_total = []

    # iterate over every codon in the sequence, identify positions for each species
    while k <= seqlength:

        ## get all the codons from gorup1 (needed for later)
        codonsg1 = []
        for p in seqgroup1:
            codonsg1.append(seqgroup1[p][(k-3):k])
        ## get all the codons from gorup2 (needed for later)
        codonsg2 = []
        for q in seqgroup2:
            codonsg2.append(seqgroup2[q][(k-3):k])

        codons = [] # make list that will hold all the codons
        for o in range(0,2): # get the current codon for each sequence
            codons.append(compgroup[o][(k-3):k]) # get the current codon for each sequence

        ## begins iteration through positions in the codon
        for i in range(0,3):

            #defines the position of each site
            ntpos = (k-(2-i))

            # looks for differences at each position in the codon
            # now it has to go through twice, doing the recpiprical for both species to make sure they both check out. any ambiguities get recorded as snyonymous (most conservative)
            syn = 0
            nonsyn = 0
            a = ['codonsg1', 'codonsg2']
            b = [1,2]

            # this will find the postions for the two groups

            # this will find the postions for the first group
            if not all(x[i] == codonsg1[0][i] for x in codonsg1): # are all nt at that position equal to the first entry in the sequence?
                unint = [] # makes blank list for the different nucleotides
                for x in codonsg1:
                    if not x[i] == codonsg1[0][i]:
                        unint.append(x[i]) # adds all different nuclotides to a list
                unint = list(set(unint)) # gets all the unique values for the list

                # change the postion in the test codon to be that of the new nucleotide
                ## to iterate for all unique positions
                for n in range(0,len(unint)):
                    # must exist as a list of characters so it can be edited
                    testcodon = list(codonsg1[0])
                    testcodon[i] = unint[n]
                    testcodon = ''.join(testcodon)

                    # sorts changes according to aa translation
                    if trans(testcodon) == trans(codonsg1[0]): # are the codons equal?
                        poly_syn_g1.append(ntpos) # writes postion to this list
                    else:
                        poly_non_g1.append(ntpos) # writes postion to this list

            # this will find the postions for the second group
            if not all(x[i] == codonsg2[0][i] for x in codonsg2): # are all nt at that position equal to the first entry in the sequence?
                unint = [] # makes blank list for the different nucleotides
                for x in codonsg2:
                    if not x[i] == codonsg2[0][i]:
                        unint.append(x[i]) # adds all different nuclotides to a list
                unint = list(set(unint)) # gets all the unique values for the list

                # change the postion in the test codon to be that of the new nucleotide
                ## to iterate for all unique positions
                for n in range(0,len(unint)):
                    # must exist as a list of characters so it can be edited
                    testcodon = list(codonsg2[0])
                    testcodon[i] = unint[n]
                    testcodon = ''.join(testcodon)

                    # sorts changes according to aa translation
                    if trans(testcodon) == trans(codonsg2[0]): # are the codons equal?
                        poly_syn_g2.append(ntpos) # writes postion to this list
                    else:
                        poly_non_g2.append(ntpos) # writes postion to this list

        # advance to the next codon
        k = k + 3

    # build the unique lists
    uni_syn = []
    uni_syn.append(poly_syn_g1)
    uni_syn.append(poly_syn_g2)
    uni_syn = [val for sublist in uni_syn for val in sublist]
    syn_dups = list(set([x for x in uni_syn if uni_syn.count(x) > 1])) ## makes the duplicates list
    poly_syn_total = list(set(uni_syn))

    # same thing for the duplicates
    uni_non = []
    uni_non.append(poly_non_g1)
    uni_non.append(poly_non_g2)
    uni_non = [val for sublist in uni_non for val in sublist]
    non_dups = list(set([x for x in uni_non if uni_non.count(x) > 1]))
    poly_non_total = list(set(uni_non))

    ##### check the sites that are the same between each species, and determine if they have the same polymorphisms (only add it once) or different polymorphisms (add another entry)

    # need to turn those dictionaries into lists of the sequences
    seqs1 = []
    for p in seqgroup1:
        seqs1.append(seqgroup1[p])

    seqs2 = []
    for p in seqgroup2:
        seqs2.append(seqgroup2[p])

    # are there polymorphisms presenent in one species that are not present in the other? if so, add another entry for that position
    for i in syn_dups:
        pos1 = []
        pos2 = []
        for x in range(0,len(seqs1)):
            pos1.append(seqs1[x][i-1])
        pos1 = list(set(pos1))
        for z in range(0,len(seqs2)):
            pos2.append(seqs2[z][i-1])
        pos2 = list(set(pos2))

        pos1.sort()
        pos2.sort()

        # if the two lists are not the same, add another position
        if not pos1 == pos2:
            poly_syn_total.append(i)

    # same for non-syononymous
    for i in non_dups:
        pos1 = []
        pos2 = []
        for x in range(0,len(seqs1)):
            pos1.append(seqs1[x][i-1])
        pos1 = list(set(pos1))
        for z in range(0,len(seqs2)):
            pos2.append(seqs2[z][i-1])
        pos2 = list(set(pos2))

        pos1.sort()
        pos2.sort()

        # if the two lists are not the same, add another position
        if not pos1 == pos2:
            poly_non_total.append(i)

    # return every list
    return poly_syn_g1, poly_non_g1, poly_syn_g2, poly_non_g2, poly_syn_total, poly_non_total

def fixedsite(seqgroup1, seqgroup2, seqlength):
    # input is a dictionary of ID : seq, length of the sequence. Output is 3 lists, synonymous fixed changes and then non-synonymous fixed changes, and the last is the non-synonymous fixed changes adjusting for initial slicing

    # counting variable
    k = 3

    # get two sequences (one from each dictionary) together to compare
    compgroup=[]
    compgroup.append(seqgroup1[list(seqgroup1.keys())[0]])
    compgroup.append(seqgroup2[list(seqgroup2.keys())[0]])

    # lists for holding the positions
    synon_list = []
    nonsynon_list = []

    # iterate over every codon in the sequence
    while k <= seqlength:

        ## get all the codons from gorup1 (needed for later)
        codonsg1 = []
        for p in seqgroup1:
            codonsg1.append(seqgroup1[p][(k-3):k])
        ## get all the codons from gorup2 (needed for later)
        codonsg2 = []
        for q in seqgroup2:
            codonsg2.append(seqgroup2[q][(k-3):k])

        codons = [] # make list that will hold all the codons
        for o in range(0,2): # get the current codon for each sequence
            codons.append(compgroup[o][(k-3):k]) # get the current codon for each sequence

        ## begins iteration through positions in the codon
        for i in range(0,3):

            #defines the position of each site
            ntpos = (k-(2-i))

            # looks for differences at each position in the codon
            # now it has to go through twice, doing the recpiprical for both species to make sure they both check out. any ambiguities get recorded as snyonymous (most conservative)
            syn = 0
            nonsyn = 0
            for s in range(0,2):
                if not all(x[i] == codons[s][i] for x in codons): # are all nt at that position equal to the first entry in the sequence?
                    unint = [] # makes blank list for the different nucleotides
                    for x in codons:
                        if not x[i] == codons[s][i]:
                            unint.append(x[i]) # adds all different nuclotides to a list
                    unint = list(set(unint)) # gets all the unique values for the list

                    # change the postion in the test codon to be that of the new nucleotide
                    ## to iterate for all unique positions
                    for n in range(0,len(unint)):
                        # must exist as a list of characters so it can be edited
                        testcodon = list(codons[s])
                        testcodon[i] = unint[n]
                        testcodon = ''.join(testcodon)

                        # sorts changes according to aa translation
                        if trans(testcodon) == trans(codons[s]):  # are the codons equal?
                            # check all entries from both groups against each other for this postion
                            if not any(codonsg1[x][i] == codonsg2[z][i] for x in range(0,len(codonsg1)) for z in range(0,len(codonsg2))):
                                syn = syn + 1

                        elif trans(testcodon) != trans(codons[s]):
                            if not any(codonsg1[x][i] == codonsg2[z][i] for x in range(0,len(codonsg1)) for z in range(0,len(codonsg2))):
                                nonsyn = nonsyn + 1

            # this checks the variables for recriprical ID of nonsynonymous position
            if nonsyn == 2:
                nonsynon_list.append(ntpos) # writes postion to this list
            elif nonsyn == 1 and syn == 1:
                synon_list.append(ntpos) # writes postion to this list
            elif syn == 2:
                synon_list.append(ntpos) # writes postion to this list



        # advance to the next codon
        k = k + 3

    ## adjust the positions in the list based on where the slicing started
    if bool(usr_startpos):
        nonsynon_listx = [i+(usr_startpos - 1) for i in nonsynon_list]
    else:
        nonsynon_listx = nonsynon_list

    return synon_list, nonsynon_list, nonsynon_listx

def polarizer(seqdic1, seqdic2, polarseq, Ds, Dn):
    # input: two sequence dictionsaries, the polarizing sequence (as a dictionary), and lists of all the classes of sites
    # output: variables that are the number of sites in each category

    # turn the dictionaries into sequences
    seqs1 = []
    for p in seqdic1:
        seqs1.append(seqdic1[p])
    seqs2 = []
    for p in seqdic2:
        seqs2.append(seqdic2[p])
    polar = []
    for p in polarseq:
        polar.append(polarseq[p])

    # call the sub-function for synonymous and then non-synonymous
    g1Ds, g2Ds, g1Dslist, g2Dslist = polar2(Ds, seqs1, seqs2, polar)
    g1Dn, g2Dn, g1Dnlist, g2Dnlist = polar2(Dn, seqs1, seqs2, polar)


    return g1Ds, g2Ds, g1Dn, g2Dn, g1Dslist, g2Dslist, g1Dnlist, g2Dnlist

def polar2(tyoe, seqs1, seqs2, polar):
    # second part of the polarizing script. outputs two numerical variables
    g1 = 0
    g2 = 0
    pos1x = []
    pos2x = []

    for i in tyoe:
        # get the position from each sequence
        pos1 = []
        for x in range(0,len(seqs1)):
            pos1.append(seqs1[x][(i-1)])
        pos2 = []
        for x in range(0,len(seqs2)):
            pos2.append(seqs2[x][(i-1)])
        pospol = []
        pospol.append(polar[0][(i-1)])

        # reduce them to their unique values
        pos1 = list(set(pos1))
        pos2 = list(set(pos2))

        # ask which variable is equal to the polarizing sequence, and assign the change to the other set
        if pos1 == pospol:
            g2 = g2 + 1 # counting how many
            pos2x.append(i) # also records the position
        elif pos2 == pospol:
            g1 = g1 + 1
            pos1x.append(i)
        else:
            g1 = g1 + 0.5
            g2 = g2 + 0.5

        ## adjust the positions in the list based on where the slicing started
        if bool(usr_startpos):
            pos1jx = [i+(usr_startpos-1) for i in pos1x]
            pos2jx = [i+(usr_startpos-1) for i in pos2x]
        else:
            pos1jx = pos1x
            pos2jx = pos2x

    return g1, g2, pos1jx, pos2jx

def fepval(Ps, Pn, Ds, Dn):
    # calculates the fischers exact p-value for the 4 values, returns float
    n = Ds + Ps + Dn + Pn
    # wrote the factorial expression this way, didn't want to re-write it
    a = Ds
    b = Ps
    c = Dn
    d = Pn
    # sad day. this often gets too big and returns infinity :(. Have to use scipy
    # fexpval = (factorial(a+b)*factorial(c+d)*factorial(a+c)*factorial(b+d))/(factorial(a)*factorial(b)*factorial(c)*factorial(d)*factorial(n))
    oddsratio, fexpval = stats.fisher_exact([[a,b ], [c, d]])
    return fexpval

def factorial(n):
    # the equivalent of n!
    if n <= 0:
        return 1
    else:
        return n * factorial(n-1)

def alfa(Ps, Pn, Ds, Dn):
    # returns the alpha value for the MK test
    alfa = 1 - ((Ds*Pn)/(Dn*Ps))
    return alfa

def summary_print(poly_syn, poly_non, fixed_syn, fixed_nonsyn, fixed_list):

    ## takes 4 numbers and a list of the fixed non-synonymous nucleotide positions

    fixed_aa_pos = [math.floor((i + 2) / 3) for i in fixed_list]
    print(poly_syn, "polymorphic synonymous changes within species (Ps)")
    print(poly_non,"polymorphic non-synonymous changes within species (Pn)")
    print(fixed_syn,"fixed synonymous changes between species (Ds)")
    print(fixed_nonsyn,"fixed non-synonymous changes between species (Dn)")
    print("Nucleotide positions:")
    print(fixed_list)
    print("Amino acid fixed non-synonymous change postions:")
    print(fixed_aa_pos)
    print("p-value =",fepval(poly_syn,poly_non,fixed_syn,fixed_nonsyn))
    print("alpha=",alfa(poly_syn,poly_non,fixed_syn,fixed_nonsyn))
    print('')

##############################
## calling the functions
##############################

# check if there is a start and stop
usr_startpos = is_it_int(usr_sttpos)
usr_endpos = is_it_int(usr_enos)

# set the groups equal to the output of the loadfile function
group1, group2, polarize = loadfile(filepath, ctlfile)

# get the length of the longest value (they are all the same length)
length = len(sorted(group1.values(), key=len)[-1])

# get the polymorphic sites
spec1_syn, spec1_nonsyn, spec2_syn, spec2_nonsyn, poly_syn, poly_non = polysite(group1, group2, length)

# get the fixed sites
fixed_syn, fixed_nonsyn, fixed_nonsyn_adj = fixedsite(group1, group2, length)

# prints the output to the terminal
print("\nUnpolarized MK")
summary_print(len(poly_syn), len(poly_non), len(fixed_syn), len(fixed_nonsyn_adj), fixed_nonsyn_adj)

# polarize the test if there is a polarizing group
if bool(polarize):

    # function to run the polarizing
    g1Ds, g2Ds, g1Dn, g2Dn, g1Dslist, g2Dslist, g1Dnlist, g2Dnlist = polarizer(group1, group2, polarize, fixed_syn, fixed_nonsyn)
    print("\nPolarized of Group 1")
    summary_print(len(spec1_syn), len(spec1_nonsyn), g1Ds, g1Dn, g1Dnlist)
    print("\nPolarized of Group 2")
    summary_print(len(spec2_syn), len(spec2_nonsyn), g2Ds, g2Dn, g2Dnlist)
